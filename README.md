# QE modified epsilon


The modified Fortran code for the ` epsilon.x ` program in the Quantum ESPRESSO suite. Originally intended to provide the ab-initio dielectric function and joint density of states, with these modifications epsilon.x will instead output ` .csv ` files containing the dipole matrix for every k-point of the calculation, and for every band-pair, as well as the jDOS for every band-pair in the directory from which it was called. These modifications might invalidate the original intended output of ` epsilon.x `, so use the modified executable only for this special purpose.

The code was taken from Quantum ESPRESSO version 6.7, so the safest way to compile a working executable is to compile and verify the PW and PP packages of this version, then replace the file epsilon.f90 in the ` PP/src ` directory with the one from this repository and execute a ` make epsilon.x ` in that directory.
